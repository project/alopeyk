 /**
 * @file
 * A JavaScript file for the Alopeyk module.
 * This Module Developed to integrate Alopeyk (An Iranian Shipping Service) Service API to Drupal 7.0
 * Make and Developed by "Ali Khalegatan" in IranDrupal.com Laboratory.
 * Contact us as "ali.khaleghian@gmail.com" or "info@IranDrupal.com"
 *
 **/

(function($){
	
	Drupal.behaviors.alopeyk = {
		attach: function (context, settings) {
			var markerGroup;
			
			$('#setting_mapid_wrapper').once('setting_mapid_wrapper',function(){

				var setting_mapid = L.map('setting_mapid').setView([Drupal.settings.alopeyk.alopeykapi_address_lat,Drupal.settings.alopeyk.alopeykapi_address_lng], 13);

				L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
					maxZoom: 18,
				}).addTo(setting_mapid);
				
				markerGroup	= L.layerGroup().addTo(setting_mapid);
				L.marker([Drupal.settings.alopeyk.alopeykapi_address_lat,Drupal.settings.alopeyk.alopeykapi_address_lng]).addTo(markerGroup)
					.bindPopup(Drupal.t("<b>Select your origin location (store).</b>")).openPopup();;
				var popup = L.popup();
				
				var inputbox = $('input[name="alopeykapi_address_lat_lng"]');
				setting_mapid.on('click',function(e){
					onMapClick(e);
					var selector = inputbox;
					set_inputbox_latlng(selector,e.latlng.lat,e.latlng.lng);
				});
				
			});			
			
			
			
			$('#mapid_wrapper').once('mapid_wrapper',function(){

				var mymap = L.map('mapid').setView([Drupal.settings.alopeyk.alopeykapi_address_lat,Drupal.settings.alopeyk.alopeykapi_address_lng], 13);

				L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
					maxZoom: 18,
					attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
						'Powered for Alopeyk Service by © <a href="http://www.IranDrupal.com/">IranDrupal.com</a>',
					id: 'mapbox.streets'
				}).addTo(mymap);
				
				mymap.addControl( new L.Control.Search({
					url: 'http://nominatim.openstreetmap.org/search?format=json&q={s}',
					jsonpParam: 'json_callback',
					propertyName: 'display_name',
					propertyLoc: ['lat','lon'],
					marker: L.circleMarker([0,0],{radius:30}),
					autoCollapse: true,
					autoType: false,
					minLength: 2
				}) );
				
				markerGroup	= L.layerGroup().addTo(mymap);
				L.marker([Drupal.settings.alopeyk.alopeykapi_address_lat,Drupal.settings.alopeyk.alopeykapi_address_lng]).addTo(markerGroup)
					.bindPopup(Drupal.t("<b>Select your location:</b>")).openPopup();;

				var popup = L.popup();
				var inputbox = $('input[name="customer_profile_billing[field_commerce_customer_latlng][und][0][value]"]');
				mymap.on('click',function(e){
					onMapClick(e);
					var selector = inputbox;
					set_inputbox_latlng(selector,e.latlng.lat,e.latlng.lng);
				});

			});
			
			$('#mapid_prelatlog_wrapper').once('mapid_prelatlog_wrapper',function(){

				if($('#mapid_prelatlog').attr('data-lat')){
					x = $('#mapid_prelatlog').attr('data-lat');
					y = $('#mapid_prelatlog').attr('data-log');
				} else {
					x = Drupal.settings.alopeyk.alopeykapi_address_lat;
					y = Drupal.settings.alopeyk.alopeykapi_address_lng;
				}

				var mymap = L.map('mapid_prelatlog').setView([x,y], 13);

				L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
					maxZoom: 18,
					attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
						'Powered for Alopeyk Service by © <a href="http://www.IranDrupal.com/">IranDrupal.com</a>',
					id: 'mapbox.streets'
				}).addTo(mymap);

				markerGroup	= L.layerGroup().addTo(mymap);
				set_address_on_map(x,y);
			});			

			function set_address_on_map(x,y) {
				markerGroup.clearLayers();
				var address_array = [x,y];
				marker_new = L.marker(address_array).addTo(markerGroup);
				x = (x).toString();
				y = (y).toString();
				marker_new.bindPopup('<div style="text-align:right; direction:rtl;"><br>'+Drupal.t('Coordinates:')+'<br></div>'+ x +', '+ y).openPopup();
				get_address_array(x,y);
			}

			function onMapClick(e) {
				markerGroup.clearLayers();
				marker_new = L.marker(e.latlng).addTo(markerGroup);
				x = (e.latlng.lat).toString();
				y = (e.latlng.lng).toString();
				marker_new.bindPopup('<div style="text-align:right; direction:rtl;"><br>'+Drupal.t('Coordinates:')+'<br></div>'+ x +', '+ y).openPopup();
				get_address_array(x,y);
			}
			
			function set_inputbox_latlng(selector,x,y) {			
				latlng_input = $(selector);
				$(latlng_input).val(x +','+ y);
				$(latlng_input).blur();
			}
			
			function get_address_array(x,y) {

				$.getJSON(
					'https://nominatim.openstreetmap.org/reverse',
					{
						format : "jsonv2",
						lat : x,
						lon : y,
						zoom : 18,
						addressdetails : 1,
						'accept-language' : 'fa',
					},
					function(data) {
						address_string = "";
						if(!!data.address.city) { address_string += data.address.city + '، '; }
						if(!!data.address.neighbourhood) { address_string += data.address.neighbourhood + '، '; }
						if(!!data.address.road) { address_string += data.address.road; }
						marker_new.bindPopup('<div style="text-align:right; direction:rtl;"><strong>'+address_string+'</strong><br>'+Drupal.t('Coordinates:')+'<br></div>'+x+', '+y).openPopup();
					}
				);

			}
			
		}
	};

	
})(jQuery);
