Wellcome to Alopeyk Module for Drupal 7.0
This Module Developed to integrate Alopeyk (An Iranian Shipping Service) Service API to Drupal 7.0

Make and Developed by "Ali Khalegatan" in IranDrupal.com Laboratory.
Contact us as "ali.khaleghian@gmail.com" or "info@IranDrupal.com"

This module make on Leafletjs.com map platform and OpenStreetMap.com whithout any need to any mony service and is completely free.
This is support multilanguage completely.

به ماژول الوپیک خوش آمدید.
بعد از نصب ماژول به صفحه تنظیمات بروید و مغادیر اولیه و توکن الوپیک را تنظیم بفرمایید.
این ماژول در بخش نقشه از سرویس های کدباز استفاده میکند و نیازی به سرویس های پرداختی ندارد.

این ماژول درخواستها را در فرایند زیر مدیریت کرده و قیمت نهایی را در صفحه پیش نمایش پرداخت (Checkout review)، به سبد خرید اضافه مینماید.

1. ارسال مختصات مقصد به الوپیک و دریافت امکان پذیر بودن خدمات در مختصات درخواستی.
2. تنظیم سقف مجموع قیمت سفارش جهت اعمال هزینه پیک به سفارش (برای سفارشهای بالای یک مجموع قیمت مشخص هزینه پیک رایگان در نظر گرفته میشود)
3. افزودن ردیف پیک به سبد خرید
4. اگر مختصات خارج از محدوده سرویس دهی الوپیک باشد یک ردیف قیمت پست با مقدار قیمت ثابت که در تنظیمات تعیین میگردد به سید خرید اضافه میشود.
5. اگر قیمت مجموع سبد خرید از سقف در نظر گرفته شده برای اعمال هزینه پیک بیشتر باشد، قیمت پیک و پست به صورت صفر (رایگان) در نظر گرفته شده و به سبد خرید اضافه میشود.
6. در نقشه مشتری امکان جستجو وجود دارد.
7. ماژول Commerce fees برای این ماژول پیش نیاز است و در ابتدای نصب دو نوع فی (Fee) به دروپال شما اضافه میشود.
8. در ابتدای نصب یک فیلد متنی به Entity type پروفایل مشتری جهت نگهداری مختصات آدرس مشتری اضافه میشود.
9. همچنین این ماژول با ماژول Commerce Addressbook سازگار است.

این ماژول در آژمایشگاه توسعه نرم افزار ایران دروپال طراحی و پیاده سازی شده است.
در صورت هرگونه سوال با ایمیلهای زیر تماس بگیرید.

نقشه راه آینده:
1. امکان ایجاد و ارسال خودکار سفارش پیک به الوپیک در صورت درخواست مشتری و پایان خرید.

ali.khaleghian@gmail.com
info@IranDrupal.com